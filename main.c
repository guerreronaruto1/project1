#include <stdio.h>

int main()
{
    /*Se declara la variable de almacenamiento necesaria. */
    int datoEntero;
    long datoEnteroLargo;
    short datoEnteroCorto;
    float datoFlotante;
    double datoDoble;
    char datoCaracter;
    /*Pedimos el dato al ususario para ser almacenado.
      Guardamos el dato escrito por el usuario para su futuro uso.  */
    printf("Escriba un valor entero: ");
    scanf("%d",&datoEntero);
    /*Dato long*/
    printf("\nEscriba un valor entero largo: ");
    scanf("%d",&datoEnteroLargo);
    /*Dato short*/
    printf("\nEscriba un valor entero corto: ");
    scanf("%d",&datoEnteroCorto);
    /*Dato float*/
    printf("\nEscriba un valor flotante: ");
    scanf("%f",&datoFlotante);
    /*Dato doble*/
    printf("\nEscriba un valor doble: ");
    scanf("%f",&datoDoble);
    /*Dato char*/
    printf("\nEscriba un Carater: ");
    scanf("%c",&datoCaracter);
    /*Presentamos al ususario el valor guardadado previamente. */
    printf("El valor entero es: %d \n", datoEntero);
    printf("El valor entero largo es: %d \n", datoEnteroLargo);
    printf("El valor entero corto es: %d \n", datoEnteroCorto);
    printf("El valor flotante es: %f \n", datoFlotante);
    printf("El valor doble es: %f \n", datoDoble);
    printf("Tu caracter escrito es: %c \n", datoCaracter);
    /*Terminamos el programa con el estado "finalizado normalmente". */
   return 0;
}
